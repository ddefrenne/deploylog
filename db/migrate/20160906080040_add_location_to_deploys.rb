class AddLocationToDeploys < ActiveRecord::Migration[5.0]
  def change
    add_column :deploys, :location, :string
  end
end
