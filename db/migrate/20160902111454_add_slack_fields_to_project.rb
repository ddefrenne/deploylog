class AddSlackFieldsToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :slack_token, :string
    add_column :projects, :slack_channel, :string
  end
end
