class AddExternalApiKeyToProjects < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :external_api_key, :string
  end
end
