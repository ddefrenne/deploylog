class CreateDeploys < ActiveRecord::Migration[5.0]
  def change
    create_table :deploys do |t|
      t.string :revision, null: false
      t.belongs_to :project, foreign_key: true
      t.string :environment
      t.string :deployer

      t.timestamps
    end
  end
end
