require "test_helper"

class Api::DeploysControllerTest < ActionController::TestCase
  test "returns 404 when no project is found" do
    post(:create, params: { project_key: "abc" })

    assert_response :missing
  end

  test "creation of a deploy" do
    project = Project.create(name: "test")

    post(
      :create,
      params: {
        project_key: project.api_key,
        revision: "abc",
        location: "web-001.server.com"
      }
    )

    assert project.deploys.count == 1
    assert project.deploys.last.revision == "abc"
    assert project.deploys.last.location = "web-001.server.com"
  end

  test "failed deploy when no revision is given" do
    project = Project.create!(name: "test")

    post(:create, params: { project_key: project.api_key })

    assert_response :error
    assert JSON.parse(@response.body)["revision"].include?("can't be blank")
  end

  test "finds project" do
    project = Project.create(name: "test")
    post(
      :create,
      params: {
        project_key: project.api_key,
        revision: "abc"
      }
    )

    assert assigns(:project) == project
  end
end
