class Gitlab::CommitFetcher
  def initialize(project)
    @project = project
  end

  def fetch_commits_between(from, to)
    client.compare(@project.gitlab_id, from, to).commits
  end

  def client
    @client ||= Gitlab.client(
      endpoint: [@project.gitlab_url, "/api/v3"].join,
      private_token: @project.external_api_key
    )
  end
end
