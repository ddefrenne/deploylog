class SlackPosterJob < ApplicationJob
  queue_as :default

  def perform(deploy)
    return true unless deploy.project.slack_token.present? && deploy.project.slack_channel.present?
    return Rails.logger.info(">>> Slack deploy notification for deploy: #{id} <<<") unless Rails.env.production?
    Slack::Web::Client.new(token: deploy.project.slack_token).chat_postMessage(
      channel: deploy.project.slack_channel,
      text: "*#{deploy.deployer}* just deployed *#{deploy.project.name}* to *#{deploy.where}*: #{Rails.application.secrets.full_host}/deploys/#{deploy.id}",
      as_user: true
    )
  end
end
