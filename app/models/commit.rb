class Commit < ApplicationRecord
  belongs_to :deploy
  has_one :project, through: :deploy

  validates :sha, :short_sha, presence: true
  validates :author_name, :author_email, presence: true
  validates :title, presence: true

  scope :search, ->(query) { where("sha = :term OR short_sha LIKE :term", term: "#{query}%") }
end
