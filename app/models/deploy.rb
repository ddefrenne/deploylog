class Deploy < ApplicationRecord
  paginates_per 20

  belongs_to :project
  has_many :commits, dependent: :destroy

  validates :revision, presence: true

  scope :sorted, -> { order(created_at: :desc) }

  after_create :queue_commit_fetching
  after_create :notify_slack

  def description
    "#{revision} on #{environment} at #{created_at.to_s(:db)}"
  end

  def fetch_commits
    return true if previous_sha.nil?
    Gitlab::CommitFetcher.new(project).fetch_commits_between(previous_sha, revision).each do |gitlab_commit|
      commits.create_with(
        short_sha: gitlab_commit["short_id"],
        author_name: gitlab_commit["author_name"],
        author_email: gitlab_commit["author_email"],
        title: gitlab_commit["title"]
      ).find_or_create_by(sha: gitlab_commit["id"])
    end
  end

  def previous_sha
    project.deploys
      .where(environment: environment)
      .where("created_at < ?", created_at)
      .order(created_at: :desc)
      .first.try(:revision)
  end

  def where
    where = environment
    where += " on #{location}" if location.present?
    where
  end

private

  def queue_commit_fetching
    return true if project.external_api_key.blank?
    CommitFetcherJob.perform_later(self)
  end

  def notify_slack
    SlackPosterJob.perform_later(self)
  end
end
