class SessionsController < ApplicationController
  skip_before_action :user_required!, only: [:new, :create, :failure, :login]

  def new
  end

  def create
    auth_hash = request.env["omniauth.auth"]
    user = User.create_with(
      email: auth_hash.info.email,
      name: auth_hash.info.email.split("@").first
    ).find_or_create_by!(uid: auth_hash.uid)
    session[:user_id] = user.id
    redirect_to "/"
  end

  def failure
    render plain: "failure!"
  end

  def destroy
    session[:user_id] = nil
    redirect_to "/"
  end

  def login
  end
end
