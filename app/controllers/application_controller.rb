class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :user_required!

  def home
    render "sessions/new"
  end

protected

  def user_required!
    return true if current_user
    redirect_to :login, error: "You need to be logged in!"
  end

  def current_user
    return false unless session[:user_id].present?
    @current_user ||= User.find(session[:user_id])
  end
  helper_method :current_user
end
