class SearchesController < ApplicationController
  def show
    @commits = Commit.search(params[:query])
    @projects = Project.search(params[:query])
  end
end
