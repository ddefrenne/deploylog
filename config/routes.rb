Rails.application.routes.draw do
  root to: "projects#index"

  resources :deploys, only: :show
  resources :projects
  resource :search, only: :show

  namespace :api do
    resources :deploys, only: :create
  end

  match '/auth/:service/callback', to: 'sessions#create', via: [:get, :post]
  get '/auth/failure', to: 'sessions#failure', via: [:get, :post]
  get '/logout', to: 'sessions#destroy'
  get "/login", to: "sessions#new", as: :login
end
