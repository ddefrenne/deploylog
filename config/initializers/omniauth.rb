Rails.application.config.middleware.use OmniAuth::Builder do
  provider :openminds, ENV["OMNIAUTH_OPENMINDS_APP_ID"], ENV["OMNIAUTH_OPENMINDS_SECRET"]
end
